//
//  ViewController3.swift
//  MobileDeviceProject
//
//  Created by Tal Weiss on 4/28/16.
//  Copyright © 2016 Tal Weiss. All rights reserved.
//THIS IS THE VIEW CONTROLLER FOR THE CALL PUBLIC SAFETY VIEW
//THIS VIEW WILL HAVE 2 BUTTONS ONE FOR CALLING NON-EMERGENCY
//AND ONE FOR CALLING EMERGENCY

import UIKit

class ViewController3: UIViewController {
    @IBOutlet weak var emergencyBtn: UIButton!
    @IBOutlet weak var nonEmergencyBtn: UIButton!
    var popUp: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.title = "Call Public Safety" // gives title on navigation bar
        emergencyBtn.layer.cornerRadius = 50
        nonEmergencyBtn.layer.cornerRadius = 50
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //link the call buttons to one function
    //you can use a switch statement to determine what number to dial
    //better than having 2 functions!
    @IBAction func CallPublicSafety(_ sender: UIButton) {
        
        let callType = sender.currentTitle!//constant with already unwrapped string
        print(callType)
        switch callType {
        
        case "EMERGENCY"://Emergency case
            let phoneNum = 15164636789 //Hofstra public safety emergency num
            if let url = URL(string:"tel://\(phoneNum)"){
                UIApplication.shared.openURL(url)
            }
            
        case "NON-EMERGENCY"://Non-Emergency case
            let phoneNum = 5164636606 //Public safety non-emergency number
            if let url = URL(string:"tel://\(phoneNum)"){
                UIApplication.shared.openURL(url)
            }
            
        
        default:
            //default case does nothing 
            break
        
        }
    }
    
}
