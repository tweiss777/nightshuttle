//
//  ViewController.swift
//  MobileDeviceProject
//
//  Created by Tal Weiss on 4/5/16.
//  Copyright © 2016 Tal Weiss. All rights reserved.
//
//THIS IS THE VIEW CONTROLLER FOR THE MAIN MENU

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var callNightShuttleBtn: UIButton!
    @IBOutlet weak var callPSafetyButton: UIButton!
    @IBOutlet weak var viewMapBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        renderButtons()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func renderButtons(){
        callNightShuttleBtn.layer.cornerRadius = 50
        callPSafetyButton.layer.cornerRadius = 50
        viewMapBtn.layer.cornerRadius = 50
    }
    
}

