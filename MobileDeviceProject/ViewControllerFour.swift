//
//  ViewControllerFour.swift
//  MobileDeviceProject
//
//  Created by Tal Weiss on 5/2/16.
//  Copyright © 2016 Tal Weiss. All rights reserved.
//VIEWCONTROLLER FOR THE MAP

import UIKit
import GoogleMaps

class ViewControllerFour: UIViewController, CLLocationManagerDelegate{
    var locationManager = CLLocationManager()
    var didFindMyLocation = false
    //this holds the map inside the subview of the map view
    @IBOutlet weak var mapSubview: GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //mapSubview.delegate = self
        navigationItem.title = "Night Shuttle Map"
        //sets the camera to fixate at certain locations (see coordinates in the code)
        let camera = GMSCameraPosition.camera(withLatitude: 40.71, longitude: -73.60, zoom: 15)
        //sets camera position within
        //this sets the camera location in the map within the subview 
        mapSubview.camera = camera
        mapSubview.accessibilityElementsHidden = true
        mapSubview.settings.compassButton = true
        mapSubview.settings.myLocationButton = true
        mapSubview.isMyLocationEnabled = true
        placeMarkers()//call to place markers
        drawRoute()//call to draw route
        
        //this will display the blue dot revealing your current location(Currently Not in use yet)
       
//        //this will drop a marker or the pin
//        let marker = GMSMarker()
//        marker.position = CLLocationCoordinate2DMake(40.715539,-73.598612)
//        marker.title = "Hempstead"
//        marker.snippet = "New York"
//        marker.map = mapSubview
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func placeMarkers(){//function to place markers on the map
        //program will read from a txt file and parse the coordinates and the marker title
        //file path and file itself are in this function
        let path = Bundle.main.path(forResource: "stops", ofType: "txt")
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: path!){
            do{
                //tries to read the file 
                let data = try NSString(contentsOfFile: path!,encoding: String.Encoding.ascii.rawValue)
                let lines = data.components(separatedBy: "\n") as [String]
                for i in 0..<lines.count-1{
                    let marker = GMSMarker.init()
                    let parts: [String] = lines[i].components(separatedBy: ":")
                    print("line \(i+1)) \(lines[i])")
                    //the next 4 lines are untested
                    let stop = parts[0]
                    let lattitude = Double(parts[1])!
                    let longitude = Double(parts[2])!
                    print("PREPARING TO ADD PLACEMARKER FOR :stop = \(stop), lattitude = \(lattitude), longitude = \(longitude)")
                    let position = CLLocationCoordinate2DMake(lattitude,longitude)
                    marker.title = stop
                    marker.position = position
                    marker.appearAnimation = kGMSMarkerAnimationPop
                    marker.map = mapSubview
                    
                }
            }
            catch let error as NSError{
                print("Error, something went wrong check your code")
                print("Error \(error)")
            }
        }
    }
    
    //function to draw route
    //this will be hardcoded
    func drawRoute(){
        let points = GMSMutablePath()//all path points plotted below
        points.add(CLLocationCoordinate2D(latitude: 40.715564 , longitude: -73.604388))//point1
        points.add(CLLocationCoordinate2D(latitude: 40.715330 , longitude: -73.604318))//point2
        points.add(CLLocationCoordinate2D(latitude: 40.715370 , longitude: -73.604111))//point3
        points.add(CLLocationCoordinate2D(latitude: 40.714353 , longitude: -73.603689))//point4
        points.add(CLLocationCoordinate2D(latitude: 40.713763 , longitude: -73.604901))//point5
        points.add(CLLocationCoordinate2D(latitude: 40.713576 , longitude: -73.605443))//point6
        points.add(CLLocationCoordinate2D(latitude: 40.714373 , longitude: -73.605791))//point7
        points.add(CLLocationCoordinate2D(latitude: 40.715914 , longitude: -73.606285))//point8
        points.add(CLLocationCoordinate2D(latitude: 40.716483 , longitude: -73.606553))//point9
        points.add(CLLocationCoordinate2D(latitude: 40.717127 , longitude: -73.607038))//point10
        points.add(CLLocationCoordinate2D(latitude: 40.717983 , longitude: -73.607476))//point11
        points.add(CLLocationCoordinate2D(latitude: 40.721089 , longitude: -73.608736))//point12
        points.add(CLLocationCoordinate2D(latitude: 40.720954 , longitude: -73.608930))//point13
        points.add(CLLocationCoordinate2D(latitude: 40.719710 , longitude: -73.610233))//point14
        points.add(CLLocationCoordinate2D(latitude: 40.717836 , longitude: -73.609514))//point15
        points.add(CLLocationCoordinate2D(latitude: 40.712678 , longitude: -73.607527))//point16
        points.add(CLLocationCoordinate2D(latitude: 40.712891 , longitude: -73.607036))//point17
        points.add(CLLocationCoordinate2D(latitude: 40.713175 , longitude: -73.606214))//point18
        points.add(CLLocationCoordinate2D(latitude: 40.709397 , longitude: -73.605119))//point19
        points.add(CLLocationCoordinate2D(latitude: 40.709365 , longitude: -73.598178))//point20
        points.add(CLLocationCoordinate2D(latitude: 40.707714 , longitude: -73.598067))//point21
        points.add(CLLocationCoordinate2D(latitude: 40.707898 , longitude: -73.592485))//point22
        points.add(CLLocationCoordinate2D(latitude: 40.709438 , longitude: -73.592389))//point23
        points.add(CLLocationCoordinate2D(latitude: 40.709586 , longitude: -73.592351))//point24
        points.add(CLLocationCoordinate2D(latitude: 40.709372 , longitude: -73.594560))//point25
        points.add(CLLocationCoordinate2D(latitude: 40.709373 , longitude: -73.597283))//point26
        points.add(CLLocationCoordinate2D(latitude: 40.709659 , longitude: -73.597317))//point27
        points.add(CLLocationCoordinate2D(latitude: 40.711121 , longitude: -73.597521))//point28
        points.add(CLLocationCoordinate2D(latitude: 40.711266 , longitude: -73.595750))//point29
        points.add(CLLocationCoordinate2D(latitude: 40.711554 , longitude: -73.592789))//point30
        points.add(CLLocationCoordinate2D(latitude: 40.711554 , longitude: -73.592694))//point31
        points.add(CLLocationCoordinate2D(latitude: 40.711644 , longitude: -73.592728))//point32
        points.add(CLLocationCoordinate2D(latitude: 40.711819 , longitude: -73.590743))//point33
        points.add(CLLocationCoordinate2D(latitude: 40.711951 , longitude: -73.589335))//point34
        points.add(CLLocationCoordinate2D(latitude: 40.712140 , longitude: -73.588427))//point35
        points.add(CLLocationCoordinate2D(latitude: 40.712185 , longitude: -73.588256))//point36
        points.add(CLLocationCoordinate2D(latitude: 40.712910 , longitude: -73.589070))//point37
        points.add(CLLocationCoordinate2D(latitude: 40.717208 , longitude: -73.592836))//point38
        points.add(CLLocationCoordinate2D(latitude: 40.716846 , longitude: -73.593895))//point39
        points.add(CLLocationCoordinate2D(latitude: 40.716805 , longitude: -73.594031))//point40
        points.add(CLLocationCoordinate2D(latitude: 40.717445 , longitude: -73.594124))//point41
        points.add(CLLocationCoordinate2D(latitude: 40.717669 , longitude: -73.594295))//point42
        points.add(CLLocationCoordinate2D(latitude: 40.715882 , longitude: -73.599303))//point43
        points.add(CLLocationCoordinate2D(latitude: 40.715626 , longitude: -73.599182))//point44
        points.add(CLLocationCoordinate2D(latitude: 40.715436 , longitude: -73.599147))//point45
        points.add(CLLocationCoordinate2D(latitude: 40.715539 , longitude: -73.598612))//point46
        points.add(CLLocationCoordinate2D(latitude: 40.715501 , longitude: -73.599146))//point47
        points.add(CLLocationCoordinate2D(latitude: 40.715685 , longitude: -73.599191))//point48
        points.add(CLLocationCoordinate2D(latitude: 40.716440 , longitude: -73.599629))//point49
        points.add(CLLocationCoordinate2D(latitude: 40.716762 , longitude: -73.599747))//point50
        points.add(CLLocationCoordinate2D(latitude: 40.717049 , longitude: -73.599924))//point51
        points.add(CLLocationCoordinate2D(latitude: 40.717524 , longitude: -73.600381))//point52
        points.add(CLLocationCoordinate2D(latitude: 40.717585 , longitude: -73.600539))//point53
        points.add(CLLocationCoordinate2D(latitude: 40.717612 , longitude: -73.600803))//point54
        points.add(CLLocationCoordinate2D(latitude: 40.717658 , longitude: -73.601518))//point55
        points.add(CLLocationCoordinate2D(latitude: 40.717800 , longitude: -73.601707))//point56
        points.add(CLLocationCoordinate2D(latitude: 40.717897 , longitude: -73.601798))//point57
        points.add(CLLocationCoordinate2D(latitude: 40.720338 , longitude: -73.602719))//point58
        points.add(CLLocationCoordinate2D(latitude: 40.720028 , longitude: -73.603993))//point59
        points.add(CLLocationCoordinate2D(latitude: 40.719875 , longitude: -73.604571))//point60
        points.add(CLLocationCoordinate2D(latitude: 40.718529 , longitude: -73.604069))//point61
        points.add(CLLocationCoordinate2D(latitude: 40.718253 , longitude: -73.604765))//point62
        points.add(CLLocationCoordinate2D(latitude: 40.715882 , longitude: -73.603361))//point63
        points.add(CLLocationCoordinate2D(latitude: 40.715644 , longitude: -73.604021))//point64
        points.add(CLLocationCoordinate2D(latitude: 40.715564 , longitude: -73.604388))//point64

        //after points plotted. this will represent our lines drawn 
        let route = GMSPolyline(path: points)
        route.map = mapSubview
        
        
        
        
        
        
        

    }
    
    //function for button in the navigation controller
    @IBAction func changeMapTypeButton(_ sender: AnyObject) {
        print("Displaying map types popup")
        //initializes the popup itself
        let changeMapPopUp = UIAlertController(title: "Map Types", message: "Select map type:", preferredStyle: UIAlertController.Style.actionSheet)
        
        //AlertAction for normal type
        let changeToNormalType = UIAlertAction(title: "Normal", style: UIAlertAction.Style.default) { (alertAction) -> Void in
            self.mapSubview.mapType = kGMSTypeNormal
        }
        
        //AlertAction for terrain type
        let changeToTerrainType = UIAlertAction(title: "Terrain", style: UIAlertAction.Style.default){ (alertAction) -> Void in
            self.mapSubview.mapType = kGMSTypeTerrain
        }
        
        //AlertAction for hybrid type
        let changeToHybridType = UIAlertAction(title: "Hybrid", style: UIAlertAction.Style.default) { (alertAction) -> Void in
            self.mapSubview.mapType = kGMSTypeHybrid
        }
        
        //AlertAction to cancel option & close menu
        let cancelOption = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel){ (alertAction) -> Void in
        }
        
        changeMapPopUp.addAction(changeToNormalType)
        changeMapPopUp.addAction(changeToTerrainType)
        changeMapPopUp.addAction(changeToHybridType)
        changeMapPopUp.addAction(cancelOption)

        
        //Displays the popup menu itself
        present(changeMapPopUp, animated: true, completion: nil)
        
    }

    
    
}




